package WallPy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Robot {
    private final int port1=5005;
    private final int port2=5005;
    UDP udpa;
    
    public void avanti(int vel){
        try {
                    udpa = new UDP("A-"+Integer.toString(vel),port1);
                } catch (IOException ex) {
                    Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
                } 
    }
    
    public  void indietro(int vel){
        try {
                    udpa = new UDP("I-"+Integer.toString(vel),port1);
                } catch (IOException ex) {
                    Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
                } 
    }
    public void destra(int vel){
        try {
                    udpa = new UDP("D-"+Integer.toString(vel),port2);
                } catch (IOException ex) {
                    Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
                } 
    }
    public void sinistra(int vel){
        try {
                    udpa = new UDP("S-"+Integer.toString(vel),port2);
                } catch (IOException ex) {
                    Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
                } 
    }
}
