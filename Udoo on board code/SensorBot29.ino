#include <Servo.h>
#include <LiquidCrystal.h>
LiquidCrystal lcd(9,8,5,4,3,2);
Servo ser1;
int waterSensor = A0; 
int tempSensor = A1;
int gasSensor = A5;
int waterValue = 0; 
int gasValue=0;
int tempValue=0;
int trigger = 12;
int echo=11;
int statusLedG=6;
int statusLedR=13;
int luxSensor=A2;
int tiltSensor=7;
int tiltValue=0;
int pos;
long duration,distance1,distance2,distance3;
void setup() {
  pinMode(trigger,OUTPUT);
  lcd.begin(16,2);
  pinMode(echo,INPUT);
  pinMode(statusLedG,OUTPUT);
  pinMode(statusLedR,OUTPUT);
  pinMode(tiltSensor,INPUT);
  Serial.begin(9600);
  ser1.attach(10);
  int ms =60000;
  int refreshms=250;
  for(int i =1;i<=60000/refreshms;i++){
   lcd.clear();
   lcd.print("Wall-Py boot...");
   lcd.blink();
   lcd.setCursor(0,1);
   ms-=refreshms;
   lcd.print("Ready in ");
   lcd.print(ms/1000);
   lcd.print("\"");
   delay(refreshms);
  }
  lcd.clear();
}

void loop() { 
  
  lcd.setCursor(0,0);
  lcd.print("Ready!");
  lcd.setCursor(0,1);
  lcd.print("Hi! I'm Wall-Py!!");
  if(Serial.available() > 0){
    char alpha = Serial.read();
    if(alpha == 'A'){
      lcd.setCursor(0,0);
      lcd.print("                ");
      lcd.setCursor(0,0);
      lcd.print("Calculating....");
      lcd.blink();
      ser1.write(20);
      delay(1000);
       digitalWrite(trigger,LOW);
    digitalWrite(trigger,HIGH);
   delayMicroseconds(10);
   digitalWrite(trigger,LOW);
   duration = pulseIn(echo,HIGH);
   distance1 = duration/58;
      ser1.write(90);
      delay(1000);  
       digitalWrite(trigger,LOW);
    digitalWrite(trigger,HIGH);
   delayMicroseconds(10);
   digitalWrite(trigger,LOW);
   duration = pulseIn(echo,HIGH);
   distance2 = duration/58;
      ser1.write(160);
      delay(1000);
       digitalWrite(trigger,LOW);
    digitalWrite(trigger,HIGH);
   delayMicroseconds(10);
   digitalWrite(trigger,LOW);
   duration = pulseIn(echo,HIGH);
   distance3 = duration/58;    
      ser1.write(90);
  int luce= analogRead(luxSensor);
  waterValue = analogRead(waterSensor); 
  gasValue= analogRead(gasSensor); 
  tempValue= readTemp(tempSensor);
  tiltValue= digitalRead(tiltSensor);
   Serial.print("{'DISSX':'");
  Serial.print(distance1);
   Serial.print("','DISC':'");
  Serial.print(distance2);
   Serial.print("','DISDX':'");
  Serial.print(distance3);
  Serial.print("','TILT':'");
  Serial.print(tiltValue);
  Serial.print("','LUCE':'" );  
  Serial.print(luce);
  Serial.print("','TEMP':'");
  Serial.print(tempValue);
  Serial.print("','GAS':'");
  Serial.print(gasValue); 
  Serial.print("','ACQUA':'");                       
  Serial.print(waterValue);
  Serial.println("'}");
  digitalWrite(statusLedR,HIGH);
  digitalWrite(statusLedG,LOW);
  delay(1000);     
  digitalWrite(statusLedR,LOW);  
  digitalWrite(statusLedG,HIGH);
  lcd.setCursor(0,0);
  lcd.print("                ");
  }
  else if (alpha == 'x'){
    lcd.setCursor(0,0);
    lcd.print("                ");
    lcd.setCursor(0,0);
    lcd.print("A* running");
    lcd.blink();
    digitalWrite(trigger,LOW);
    digitalWrite(trigger,HIGH);
    delayMicroseconds(10);
    digitalWrite(trigger,LOW);
    duration = pulseIn(echo,HIGH);
    distance1 = duration/58;
    Serial.println(distance1);
    delay(1000);
    lcd.setCursor(0,0);
  lcd.print("                ");
  }
 
  }
}


float readTemp(int pinTemp)
{
  float temp = 0.0;       // valore convertito in temperatura (°C)
  int val = 0;            // valore quantizzato dall'ADC [0..1023]
  int nread = 5;          // numero di letture (da 5 a 8)
  float somma = 0.0;      // somma delle letture
  for (int i=0; i<nread; i++)
  {
    val = analogRead( pinTemp );  
    temp = (val*3.3 /1024)*100;   // lo converte in °C
    somma += temp;                             // aggiunge alla somma delle temperature lette   
  }   
  return ( somma / nread );                     // ne calcola il valore medio 
}

