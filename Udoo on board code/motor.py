import socket
import serial
import comandi
import string

ipfile=open("IP.txt","r")
IP_UDP=ipfile.readline()
ipfile.close()
print "Motor controller ready at IP: "+IP_UDP
PORTA_UDP=5005

sock=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sock.bind((IP_UDP,PORTA_UDP))
ser = serial.Serial("/dev/ttyUSB0",57600,8,"N",1,timeout=1)

while True:
	messaggio, addr = sock.recvfrom(1024)
	messaggio = messaggio.decode('UTF-8')
	list = string.split(messaggio,"-")
	if list[0] == 'A':
	  print("AVANTI a velocita' "+ list[1])
	  ser.write(comandi.ComandiAvanti[int(list[1])])
        elif list[0] == 'I':
          print("INDIETRO a velocita' " + list[1])
	  ser.write(comandi.ComandiIndietro[int(list[1])])
	elif list[0] == 'D':
 	  print("DESTRA a velocita' "+list[1])
	  ser.write(comandi.ComandiDestra[int(list[1])])
	elif list[0] == 'S':
	  print("SINISTRA a velocita' "+ list[1])
	  ser.write(comandi.ComandiSinistra[int(list[1])])
